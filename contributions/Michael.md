# Michael Giorgio's contribution history
Hi there.

## First Week
1. Init Front End (React with Typescript) and Back End (Haskell Web Server (Happstack)) Repository
2. Create initial issues (General, Front End, and Back End)
3. Discuss with team what project to choose
4. Discuss with team web server or serverless
5. Replace existing Haskell Web Server with Serverless, AWS Lambda Repository
6. Understanding how to develop AWS Lambda repository locally, and how to deploy Lambda Functions
7. Create issues on Gitlab
8. Locally practice how to deploy functions to AWS Lambda
