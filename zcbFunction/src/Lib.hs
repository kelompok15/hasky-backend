module Lib where

import GHC.Generics
import Data.Aeson

import Data.List

import Aws.Lambda
import qualified Data.ByteString.Lazy.Char8 as ByteString

-- ZCB = faceValue / (1 + rateOfInterest) ^ timeToMaturity

data Zcb = Zcb
  { faceValue :: Double
  , maxRate :: Double
  , minRate :: Double
  , interestRate :: Double
  , maturityDate :: Int
  } deriving (Generic)
instance FromJSON Zcb
instance ToJSON Zcb

handler :: Zcb -> Context -> IO (Either String [Double])
handler zcb context =
  if (faceValue zcb) > 0 then
    return (Right (zcbTree (interestRate zcb) ((maxRate zcb)+1) (1-(minRate zcb)) (maturityDate zcb) (faceValue zcb)))
  else
    return (Left "Error: Couldn't Calculate Zero Coupon Bond")

calculateZcb :: Double -> Double -> Double -> Double
calculateZcb faceValue maturityDate interestRate  =
  faceValue / ((1 + interestRate) ** maturityDate)

-- ZCB Lattice Formula Below

takeLeftover :: [a] -> t -> [a]
takeLeftover [] _ = []
takeLeftover (x:xss) _ = xss

lastN :: Int -> [a] -> [a]
lastN n xs = foldl' takeLeftover xs (drop n xs)

firstN :: Int -> [a] -> [a]
firstN n xs = foldl' takeLeftover xs (take n xs)

slice :: [Double] -> Int -> Int -> [Double]
slice xs fromA toB =
  drop fromA . take toB $ xs

binomValues :: [Double] -> Double -> Double -> [Double]
binomValues xs max min =
  [x | x <- map (*max) xs] ++ [(last xs) * min]


latticeTree :: [Double] -> Double -> Double -> Int -> [Double]
latticeTree faceValue max min year =
  faceValue ++ (latticeTreeGenerator faceValue max min year)

latticeTreeGenerator :: [Double] -> Double -> Double -> Int -> [Double]
latticeTreeGenerator _ _ _ 0 = []
latticeTreeGenerator xs max min year = 
  (binomValues xs max min) ++
  (latticeTreeGenerator (binomValues xs max min) max min (year-1))

latticeMultiplier :: [Double] -> [Double]
latticeMultiplier xs = 
  [y | x <- xs , y <- [(1 / (1 + x))]]

faceValueLeaf :: Double -> Int -> [Double]
faceValueLeaf _ 0 = []
faceValueLeaf faceValue year =
  [faceValue] ++ (faceValueLeaf faceValue (year-1))

bondPrice :: [Double] -> [Double] -> [Double]
bondPrice latticeArr bondArr =
  zipWith (*) (latticeMultiplier latticeArr) (map (/2) (zipWith (+) bondArr (tail bondArr)))

zcbTreeGenerator :: [Double] -> Int -> [Double] -> [Double]
zcbTreeGenerator _ 0 _ = []
zcbTreeGenerator latticeT year faceValueArr =
  zcbTreeGenerator (take ((length (latticeT)) - (year)) latticeT) (year-1) (bondPrice (lastN (year) latticeT) (faceValueArr))
  ++ bondPrice (lastN (year) latticeT) (faceValueArr)

zcbTree :: Double -> Double -> Double -> Int -> Double -> [Double]
zcbTree interestRate max min year faceValue = 
  zcbTreeGenerator (latticeTree [interestRate] max min (year-1)) year (faceValueLeaf faceValue (year+1)) ++ (faceValueLeaf faceValue (year+1))