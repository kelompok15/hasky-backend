# hasky-backend

Hi, this is the Back End repository for our Hasky app. We're using Serverless Back End Service with pure Function(s) as the Service. Each Function in AWS Lambda is an individual folder in this level.

Please check out our end product below!<br/>
https://frosty-leakey-31562c.netlify.com/

## Team Members (Class B)

1. 1606875825 - Michael Giorgio

2. 1606895461 - William Rumanta

3. 1606879773 - Misael Jonathan

4. 1606918446 - Zulia Putri

5. 1606875996 - Nabila Fakhirah Yusroni

## How to deploy to AWS Lambda

Do read this to set up the project,
`https://medium.com/the-theam-journey/getting-started-with-the-haskell-aws-lambda-runtime-951b2322c7a3`

and this one to deploy it manually,
`https://theam.github.io/aws-lambda-haskell-runtime/index.html`

## API

Our API are all deployed in AWS Lambda. In our MVP product, we have 3 main APIs which serve each purposes.

### Zero Coupon Bond

A zero-coupon bond is a bond that makes no periodic interest payments and is sold at a deep discount from face value. The buyer of the bond receives a return by the gradual appreciation of the security, which is redeemed at face value on a specified maturity date.

API Url:
> https://d55ykgd34g.execute-api.us-east-1.amazonaws.com/default/zcbFunction

Request body:

```json
{
  "faceValue": 100,
  "maxRate": 0.6,
  "minRate": 0.6,
  "interestRate": 0.05,
  "maturityDate": 3
}
```

Response body:

```json
[
  86.68273877623326,
  85.9035680906042,
  96.13018333948567,
  88.65248226950354,
  96.89922480620154,
  99.20634920634922,
  100.0,
  100.0,
  100.0,
  100.0
]
```

### European Callable Bond

European callable bond is a type of bond that can be redeemed by the issuer at a predetermined date prior to the bond’s maturity date, such as the last coupon date. European callable bonds behave similarly to plain vanilla bonds after the call date, with a comparable coupon and time to maturity. European callable bonds pose interest rate risk to bondholders, although not as much as American callable bonds.

API Url:
> https://tq0lwfg1f5.execute-api.us-east-1.amazonaws.com/default/europeanNonBarrierCallOption

Request body:

```json
{
  "stockPrice": 100,
  "strikePrice": 110,
  "volatility": 0.05,
  "maxRate": 1.1,
  "minRate": 0.9,
  "yearsToMaturity": 3
}
```

Response body:

```json
[
  8.602916037831898,
  11.957336453278806,
  0.0,
  16.619701323150817,
  0.0,
  0.0,
  23.10000000000005,
  0.0,
  0.0,
  0.0
]
```

### American Callable Bond

An American Callable Bond, also known as continuously callable, is a bond that an issuer can redeem at any time prior to its maturity. Usually a premium is paid to the bondholder when the bond is called.

Api Url:
> https://ihb6rjlkzi.execute-api.us-east-1.amazonaws.com/default/americanFunction

Request body:

```json
{
  "stockPrice": 100,
  "strikePrice": 100,
  "volatility": 100000,
  "maxRate": 1.2,
  "minRate": 0.8,
  "yearsToMaturity": 3
}
```

Response body:

```json
[
  0.0,
  0.0,
  20.0,
  20.0,
  0.0,
  4.0,
  0.0,
  0.0,
  23.200000000000003,
  48.8
]
```
