module Lib where

import GHC.Generics
import Data.Aeson
import Data.List

import Aws.Lambda

data European = European
  { stockPrice :: Double
  , strikePrice :: Double
  , barrierPrice :: Double
  , volatility :: Double
  , maxRate :: Double
  , minRate :: Double
  , yearsToMaturity :: Int
  } deriving (Generic)
instance FromJSON European
instance ToJSON European

handler :: European -> Context -> IO (Either String [Double])
handler european context =
  if (stockPrice european) > 0 then
    return (Right (optionTree (stockTree (stockPrice european) (maxRate european) (minRate european) (yearsToMaturity european))
      (strikePrice european) (barrierPrice european) (yearsToMaturity european) (volatility european) (maxRate european) (minRate european)))
  else
    return (Left "Error: Couldn't Calculate European Option Stock Price")

takeLeftover :: [a] -> t -> [a]
takeLeftover [] _ = []
takeLeftover (x:xss) _ = xss

lastN :: Int -> [a] -> [a]
lastN n xs = foldl' takeLeftover xs (drop n xs)

stockTree :: Double -> Double -> Double -> Int -> [Double]
stockTree stockPrice maxRate minRate year =
    [stockPrice] ++ (generateStockTree [stockPrice] maxRate minRate year)

generateStockTree :: [Double] -> Double -> Double -> Int -> [Double]
generateStockTree _ _ _ 0 = []
generateStockTree stockPrices maxRate minRate year =
    (generateStockArray stockPrices maxRate minRate) ++
    generateStockTree (generateStockArray stockPrices maxRate minRate) maxRate minRate (year-1)

generateStockArray :: [Double] -> Double -> Double -> [Double]
generateStockArray stockPrices maxRate minRate =
    [x | x <- map (*maxRate) stockPrices] ++ [(last stockPrices) * minRate]

payOffLeaf :: Double -> Double -> Double -> Double
payOffLeaf stockPrice strikePrice barrierPrice
    | stockPrice > barrierPrice = 0
    | otherwise = max (stockPrice - strikePrice) 0

payOffInternal :: Double -> Double -> Double
payOffInternal stockPrice strikePrice = max (stockPrice - strikePrice) 0
    
calculateQProb :: Double -> Double -> Double -> Double
calculateQProb volatility maxRate minRate  =
    ((exp (volatility * 1)) - minRate) / (maxRate - minRate) -- 0.25 -> 1

arrayOf :: Double -> Int -> [Double]
arrayOf _ 0 = []
arrayOf strikePrice year =
    [strikePrice] ++ (arrayOf strikePrice (year-1))

optionTree :: [Double] -> Double -> Double -> Int -> Double -> Double -> Double -> [Double]
optionTree stockTree strikePrice barrierPrice year volatility maxRate minRate =
    (generateOptionTree (take ((length stockTree) - ((2*year)-1)) stockTree) optionLeafArray strikePrice year qProb volatility) ++ optionLeafArray -- optionLeafArray 1st = (take ((length stockTree) - (year+1)) stockTree)
    where 
        optionLeafArray = zipWith3 (payOffLeaf) (lastN (year+1) stockTree) (arrayOf strikePrice (year+1)) (arrayOf barrierPrice (year+1))
        qProb = calculateQProb volatility maxRate minRate
    
calculateOptionPrice :: Double -> Double -> Double -> Double -> Double 
calculateOptionPrice optionPriceUp optionPriceDown qProb volatility =
        ((optionPriceUp * qProb) + (optionPriceDown * (1 - qProb))) * (exp (-1 * volatility * 1)) -- 0.25 -> 1

generateOptionTree :: [Double] -> [Double] -> Double -> Int -> Double -> Double -> [Double]
generateOptionTree _ _ _ (-1) _ _ = []
generateOptionTree stockTree childOptionArray strikePrice year qProb volatility =
    (generateOptionTree (take ((length stockTree) - (year-1)) stockTree) optionArray strikePrice (year-1) qProb volatility) ++ optionArray
    where 
        optionArray = zipWith4 (calculateOptionPrice) (take ((length childOptionArray)-1) childOptionArray) (tail childOptionArray) (arrayOf qProb year) (arrayOf volatility year)
